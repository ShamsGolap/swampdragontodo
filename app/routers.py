from django.shortcuts import get_object_or_404
from swampdragon import route_handler
from swampdragon.route_handler import ModelRouter, ModelPubRouter
from app.models import Todo
from app.serializers import TodoSerializer


class TodoRouter(ModelRouter):
    route_name = 'todo-router'
    serializer_class = TodoSerializer
    model = Todo

    def get_object(self, **kwargs):
        return self.model.objects.get(pk=kwargs['id'])

    def get_query_set(self, **kwargs):
        return self.model.objects.all()

    def create(self, **kwargs):
        todo = Todo(id=kwargs['id'], label=kwargs['label'], done=False)
        todo.save()

    def update(self, **kwargs):
        todo = Todo(id=kwargs['id'], label=kwargs['label'], done=kwargs['done'])
        todo.save()

    def delete(self, **kwargs):
        todo = Todo.objects.get(pk=kwargs['id'])
        todo.delete()


route_handler.register(TodoRouter)
