# -*- coding: utf-8 -*-

from django.db import models
from swampdragon.models import SelfPublishModel
from app.serializers import TodoSerializer


class Todo(SelfPublishModel, models.Model):
    serializer_class = TodoSerializer
    label = models.CharField(max_length=200)
    done = models.BooleanField(default=False)

    def __str__(self):
        if self.done:
            return u'\" ' + self.label + u' \" IS done'
        else:
            return u'\" ' + self.label + u' \" IS NOT done'

    def __unicode__(self):
        if self.done:
            return u'\" ' + self.label + u' \" IS done'
        else:
            return u'\" ' + self.label + u' \" IS NOT done'
