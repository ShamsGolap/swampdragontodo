from swampdragon.serializers.model_serializer import ModelSerializer


class TodoSerializer(ModelSerializer):
    class Meta:
        model = 'app.Todo'
        publish_fields = ('label', 'done')
        update_fields = ('done', )
