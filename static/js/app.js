/*global angular */

(function (ng) {
    'use strict';

    ng.module('TodoApp', [
        'SwampDragonServices',
        'TodoControllers'
    ]);
}(angular));
