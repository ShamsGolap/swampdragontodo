/*globals angular, DataMapper, console, prompt, alert */

(function (ng, DataMapper) {
    'use strict';

    var TodoControllers = ng.module('TodoControllers', []);

    TodoControllers.controller('TodoCtrl', ['$scope', '$dragon', function ($scope, $dragon) {
        var // Function
            dragonReady,
            addTodo,
            toggleTodoDone,
            editTodo,
            deleteTodo,
            refreshList,

            // Attributes
            todoRouter = 'todo-router',
            editTodoNewLabel;

        dragonReady = $dragon.onReady;

        refreshList = function () {
            dragonReady(function () {
                $dragon
                    .getList(todoRouter)
                    .then(function (response) {
                        $scope.todos = response.data;
                    });
            });
        };

        addTodo = function () {
            if ($scope.newTodo) {
                $dragon.create(todoRouter, {
                    id: null,
                    label: $scope.newTodo
                });

                $scope.newTodo = '';
            }
        };

        toggleTodoDone = function (todo) {
            todo.done = !todo.done;
            $dragon.update(todoRouter, todo);
        };

        editTodo = function (todo) {
            editTodoNewLabel = prompt('Please enter the new label for the todo: ', todo.label);
            todo.label = editTodoNewLabel;
            if (editTodoNewLabel) {
                $dragon.update(todoRouter, {
                    id: todo.id,
                    label: todo.label,
                    done: todo.done
                });
            }
        };

        deleteTodo = function (todo) {
            dragonReady(function () {
                $dragon.delete(todoRouter, {
                    id: todo.id
                });
            });
        };

        $scope.todos = [];
        $scope.newTodo = '';
        $scope.channel = 'todo-channel';

        $scope.addTodo = addTodo;
        $scope.toggleTodoDone = toggleTodoDone;
        $scope.editTodo = editTodo;
        $scope.deleteTodo = deleteTodo;
        $scope.refreshList = refreshList;

        dragonReady(function () {
            $dragon
                .subscribe(todoRouter, $scope.channel, {})
                .then(function (response) {
                    $scope.dataMapper = new DataMapper(response.data);
                });

            $dragon
                .getList(todoRouter)
                .then(function (response) {
                    $scope.todos = response.data;
                });
        });

        $dragon.onChannelMessage(function (channels, message) {
            if (!ng.isArray(channels)) {
                return;
            }

            if (channels.indexOf($scope.channel) > -1) {
                $scope.$apply(function () {
                    $scope.dataMapper.mapData($scope.todos, message);
                });
            }
        });
    }]);
}(angular, DataMapper));
